﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;

namespace WindowsFormsApplication9
{
    class TableModel
    {
        // таблица данных о задаче 
        public DataTable taskStructTable = new DataTable();
        // выбранная строка 
        public int selectedRowIndex = -1; 

        // создание таблицы со столбцами 
        public void create(DataGridView table)
        {
            // добавление столбцов 
            taskStructTable.Columns.Add(getNewColumn("Операция [ID]", "id", typeof(String)));
            taskStructTable.Columns.Add(getNewColumn("Описание",   "title", typeof(String)));
            taskStructTable.Columns.Add(getNewColumn("Предшествующие операции", "dependency", typeof(String)));
            taskStructTable.Columns.Add(getNewColumn("Время выполнения",          "TimeLimit", typeof(Int32)));            
            // привязка 
            table.DataSource = taskStructTable; 
            // равномерный мод для колонок 
            table.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            table.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill; 
            table.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            table.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            // создание контекстного меню 
            ContextMenuStrip operationMenu = new ContextMenuStrip();
            ToolStripMenuItem operationAdd = new ToolStripMenuItem("Добавить");
            ToolStripMenuItem operationDelete = new ToolStripMenuItem("Удалить");
            // добавление событий нажатия 
            operationAdd.Click += contextMenuItemAddClick;
            operationDelete.Click += contextMenuItemDeleteClick; 
            
            operationMenu.Name = "operationMenu";
            operationMenu.Items.AddRange(new[] { operationAdd, operationDelete }); 
            // добавляем контестное меню для datagridview 
            table.ContextMenuStrip = operationMenu; 
            
        }
        
        
        // создает и возвращает новосозданый столбец 
        public DataColumn getNewColumn(string name, string caption, Type type)
        {
            DataColumn newColumn = new DataColumn();
            newColumn.DataType = type;
            newColumn.ColumnName = name; 
            newColumn.Caption = caption;
            newColumn.ReadOnly = false; 
            return newColumn;
        }

        public void reSet()
        {

        }

        public void contextMenuItemAddClick(object sender, EventArgs e)
        {
            DataRow newRow = taskStructTable.NewRow();
            newRow[0] = "A";
            newRow[1] = "Describe"; 
            newRow[2] = "-"; 
            newRow[3] = 5; 
            taskStructTable.Rows.Add(newRow);
        }
        
        public void contextMenuItemDeleteClick(object sender, EventArgs e)
        {
            taskStructTable.Rows[selectedRowIndex].Delete(); 
        }

    }
}
